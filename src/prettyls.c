
#ifndef PRETTYLS_C
#define PRETTYLS_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <pwd.h>
#include "colors.h"
#include "prettyls.h"


const int permissions_bits[] = {
   S_IRUSR, S_IWUSR, S_IXUSR,
   S_IRGRP, S_IWGRP, S_IXGRP,
   S_IROTH, S_IWOTH, S_IXOTH
};

char *file_size_units[] = {
   "B",  "KB", "GB",
   "MB", "TB", "PB",
   "EB", "ZB", "YB"
};







Directory_Stats new_directory(char *path) {

   DIR *dir = opendir(path);

   if (dir == NULL) {
      printf("\ninvalid path\n");
      exit(1);
   }

   Directory_Stats D;
   D.size = 0;
   D.num_dirs = 0;
   D.num_files = 0;
   D.max_file_size_strlen = 0;
   D.max_file_name_strlen = 0;
   D.max_file_owner_strlen = 0;
   D.max_time_strlen = 0;

   while (readdir(dir)) {
      D.size++;
   }

   closedir(dir);
   return D;
}


Wrapper new_wrapper(
   char *left_icon,
   char *right_icon,
   char *pad_left,
   char *pad_right,
   char *background_color) {

   Wrapper W;
   W.left_icon = (left_icon == NULL) ? "" : left_icon;
   W.right_icon = (right_icon == NULL) ? "" : right_icon;
   W.background_color = (background_color == NULL) ? "" : background_color;
   W.pad_left = (pad_left == NULL) ? 1 : strtol(pad_left, NULL, 10);
   W.pad_right = (pad_right == NULL) ? 0 : strtol(pad_right, NULL, 10);

   return W;
}



Format new_format(char *pad_left, char *pad_right, char *icon_gap) {
   Format F;
   F.pad_left = (pad_left == NULL) ? 1 : strtol(pad_left, NULL, 10);
   F.pad_right = (pad_right == NULL) ? 1 : strtol(pad_right, NULL, 10);
   F.icon_gap = (icon_gap == NULL) ? 1 : strtol(icon_gap, NULL, 10);

   return F;
}



char *set_config_str(char *envivar, char *default_str) {
   char *user_config_str = getenv(envivar);
   return (user_config_str != NULL) ? user_config_str : default_str;
}



Pretty_File_Permissions new_pretty_permissions() {
   Pretty_File_Permissions P;

   P.format = new_format(
      getenv("PLS_PERMISSIONS_PAD_LEFT"),
      getenv("PLS_PERMISSIONS_PAD_RIGHT"),
      getenv("PLS_PERMISSIONS_ICON_GAP")
   );

   P.wrapper = new_wrapper(
      getenv("PLS_PERMISSIONS_LEFT_WRAPPER"),
      getenv("PLS_PERMISSIONS_RIGHT_WRAPPER"),
      getenv("PLS_PERMISSIONS_WRAPPER_PAD_LEFT"),
      getenv("PLS_PERMISSIONS_WRAPPER_PAD_RIGHT"),
      getenv("PLS_PERMISSIONS_WRAPPER_BACKGROUND")
   );

   P.icon = set_config_str("PLS_PERMISSIONS_ICON", BOLD_MAGENTA"");
   P.bit_icons[0] = set_config_str("PLS_PERMISSIONS_READ_BIT", MAGENTA"r" );
   P.bit_icons[1] = set_config_str("PLS_PERMISSIONS_WRITE_BIT", BLUE"w" );
   P.bit_icons[2] = set_config_str("PLS_PERMISSIONS_EXECUTE_BIT", CYAN"x" );
   P.bit_icons[3] = set_config_str("PLS_PERMISSIONS_NONE_BIT", YELLOW"-" );

   return P;
}



Pretty_File_Size new_pretty_file_size() {
   Pretty_File_Size S;

   S.format = new_format(
      getenv("PLS_FILE_SIZE_PAD_LEFT"),
      getenv("PLS_FILE_SIZE_PAD_RIGHT"),
      getenv("PLS_FILE_SIZE_ICON_GAP")
   );

   S.wrapper = new_wrapper(
      getenv("PLS_FILE_SIZE_LEFT_WRAPPER"),
      getenv("PLS_FILE_SIZE_RIGHT_WRAPPER"),
      getenv("PLS_FILE_SIZE_WRAPPER_PAD_LEFT"),
      getenv("PLS_FILE_SIZE_WRAPPER_PAD_RIGHT"),
      getenv("PLS_FILE_SIZE_WRAPPER_BACKGROUND")
   );

   S.icon = set_config_str("PLS_FILE_SIZE_ICON", CYAN"");
   S.text_color = set_config_str("PLS_FILE_SIZE_TEXT_COLOR", CYAN );

   return S;
}



Pretty_File_Owner new_pretty_file_owner() {
   Pretty_File_Owner O;

   O.format = new_format(
      getenv("PLS_FILE_OWNER_PAD_LEFT"),
      getenv("PLS_FILE_OWNER_PAD_RIGHT"),
      getenv("PLS_FILE_OWNER_ICON_GAP")
   );

   O.self_wrapper = new_wrapper(
      getenv("PLS_FILE_OWNER_SELF_LEFT_WRAPPER"),
      getenv("PLS_FILE_OWNER_SELF_RIGHT_WRAPPER"),
      getenv("PLS_FILE_OWNER_SELF_WRAPPER_PAD_LEFT"),
      getenv("PLS_FILE_OWNER_SELF_WRAPPER_PAD_RIGHT"),
      getenv("PLS_FILE_OWNER_SELF_WRAPPER_BACKGROUND")
   );

   O.root_wrapper = new_wrapper(
      getenv("PLS_FILE_OWNER_ROOT_LEFT_WRAPPER"),
      getenv("PLS_FILE_OWNER_ROOT_RIGHT_WRAPPER"),
      getenv("PLS_FILE_OWNER_ROOT_WRAPPER_PAD_LEFT"),
      getenv("PLS_FILE_OWNER_ROOT_WRAPPER_PAD_RIGHT"),
      getenv("PLS_FILE_OWNER_ROOT_WRAPPER_BACKGROUND")
   );

   O.default_wrapper = new_wrapper(
      getenv("PLS_FILE_OWNER_DEFAULT_LEFT_WRAPPER"),
      getenv("PLS_FILE_OWNER_DEFAULT_RIGHT_WRAPPER"),
      getenv("PLS_FILE_OWNER_DEFAULT_WRAPPER_PAD_LEFT"),
      getenv("PLS_FILE_OWNER_DEFAULT_WRAPPER_PAD_RIGHT"),
      getenv("PLS_FILE_OWNER_DEFAULT_WRAPPER_BACKGROUND")
   );

   O.self_icon = set_config_str("PLS_FILE_OWNER_SELF_ICON", YELLOW"" );
   O.self_text_color = set_config_str("PLS_FILE_OWNER_SELF_TEXT_COLOR", YELLOW );
   O.root_icon = set_config_str("PLS_FILE_OWNER_ROOT_ICON", RED"" );
   O.root_text_color = set_config_str("PLS_FILE_OWNER_ROOT_TEXT_COLOR", RED );
   O.default_icon = set_config_str("PLS_FILE_OWNER_DEFAULT_ICON", MAGENTA"" );
   O.default_text_color = set_config_str("PLS_FILE_OWNER_DEFAULT_TEXT_COLOR", MAGENTA );

   return O;
}


Pretty_File_Name new_pretty_file_name() {
   Pretty_File_Name N;

   N.format = new_format(
      getenv("PLS_FILE_NAMES_PAD_LEFT"),
      getenv("PLS_FILE_NAMES_PAD_RIGHT"),
      getenv("PLS_FILE_NAMES_ICON_GAP")
   );

   N.wrapper = new_wrapper(
      getenv("PLS_FILE_NAMES_LEFT_WRAPPER"),
      getenv("PLS_FILE_NAMES_RIGHT_WRAPPER"),
      getenv("PLS_FILE_NAMES_WRAPPER_PAD_LEFT"),
      getenv("PLS_FILE_NAMES_WRAPPER_PAD_RIGHT"),
      getenv("PLS_FILE_NAMES_WRAPPER_BACKGROUND")
   );

   N.icons[BLOCK_DEVICE] = set_config_str("PLS_FILE_NAMES_BLK_DEVICE_ICON", BOLD_GREEN"" );
   N.icons[CHARACTER_DEVICE] = set_config_str("PLS_FILE_NAMES_CHARACTER_DEVICE_ICON", GREEN"" );
   N.icons[DIRECTORY] = set_config_str("PLS_FILE_NAMES_DIR_ICON", BOLD_BLUE"" );
   N.icons[FIFO] = set_config_str("PLS_FILE_NAMES_FIFO_ICON", BOLD_MAGENTA"" );
   N.icons[SYMLINK] = set_config_str("PLS_FILE_NAMES_SYMLINK_ICON", BOLD_CYAN"" );
   N.icons[SOCKET] = set_config_str("PLS_FILE_NAMES_SOCKET_ICON", BOLD_CYAN"" );
   N.icons[C] = set_config_str("PLS_FILE_NAMES_C_ICON", BLUE"" );
   N.icons[CPP] = set_config_str("PLS_FILE_NAMES_CPP_ICON", BLUE"" );
   N.icons[CONF] = set_config_str("PLS_FILE_NAMES_CONF_ICON", WHITE"" );
   N.icons[EXECUTABLE] = set_config_str("PLS_FILE_NAMES_EXECUTABLE_ICON", BOLD_GREEN"" );
   N.icons[JAVA] = set_config_str("PLS_FILE_NAMES_JAVA_ICON", YELLOW"" );
   N.icons[JAVASCRIPT] = set_config_str("PLS_FILE_NAMES_JAVASCRIPT_ICON", MAGENTA"" );
   N.icons[JPEG] = set_config_str("PLS_FILE_NAMES_JAVASCRIPT_ICON", MAGENTA"" );
   N.icons[JULIA] = set_config_str("PLS_FILE_NAMES_JULIA_ICON", MAGENTA"" );
   N.icons[LICENSE] = set_config_str("PLS_FILE_NAMES_LICENSE_ICON", WHITE"" );
   N.icons[LUA] = set_config_str("PLS_FILE_NAMES_LUA_ICON", CYAN"" );
   N.icons[MAKEFILE] = set_config_str("PLS_FILE_NAMES_MAKEFILE_ICON", BOLD_WHITE"" );
   N.icons[MP3] = set_config_str("PLS_FILE_NAMES_MP3_ICON", CYAN"" );
   N.icons[MP4] = set_config_str("PLS_FILE_NAMES_MP4_ICON", CYAN"" );
   N.icons[OBJECT_FILE] = set_config_str("PLS_FILE_NAMES_OBJECT_FILE_ICON", YELLOW"" );
   N.icons[PDF] = set_config_str("PLS_FILE_NAMES_PDF_ICON", WHITE"");
   N.icons[PLAIN_TEXT] = set_config_str("PLS_FILE_NAMES_PLAIN_TEXT_ICON", WHITE"");
   N.icons[PNG] = set_config_str("PLS_FILE_NAMES_PNG_ICON", MAGENTA"");
   N.icons[PYTHON] = set_config_str("PLS_FILE_NAMES_PYTHON_ICON", MAGENTA"");
   N.icons[RUST] = set_config_str("PLS_FILE_NAMES_RUST_ICON", GREEN"" );
   N.icons[VIM] = set_config_str("PLS_FILE_NAMES_VIM_ICON", GREEN"" );
   N.icons[NVIM] = set_config_str("PLS_FILE_NAMES_NVIM_ICON", GREEN"" );
   N.icons[WAV] = set_config_str("PLS_FILE_NAMES_WAV_ICON", BLUE"ﲋ" );
   N.icons[DEFAULT] = set_config_str("PLS_FILE_NAMES_DEFAULT_ICON", WHITE"" );

   N.text_colors[BLOCK_DEVICE] = set_config_str("PLS_FILE_NAMES_BLK_DEVICE_TEXT_COLOR", BOLD_GREEN );
   N.text_colors[CHARACTER_DEVICE] = set_config_str("PLS_FILE_NAMES_DIR_TEXT_COLOR", GREEN );
   N.text_colors[DIRECTORY] = set_config_str("PLS_FILE_NAMES_DIR_TEXT_COLOR", BOLD_BLUE );
   N.text_colors[SYMLINK] = set_config_str("PLS_FILE_NAMES_SYMLINK_TEXT_COLOR", BOLD_CYAN );
   N.text_colors[SOCKET] = set_config_str("PLS_FILE_NAMES_SOCKET_TEXT_COLOR", BOLD_CYAN );
   N.text_colors[FIFO] = set_config_str("PLS_FILE_NAMES_FIFO_TEXT_COLOR", BOLD_MAGENTA );
   N.text_colors[C] = set_config_str("PLS_FILE_NAMES_C_TEXT_COLOR", BLUE );
   N.text_colors[CPP] = set_config_str("PLS_FILE_NAMES_CPP_TEXT_COLOR", BLUE );
   N.text_colors[CONF] = set_config_str("PLS_FILE_NAMES_CONF_TEXT_COLOR", WHITE );
   N.text_colors[EXECUTABLE] = set_config_str("PLS_FILE_NAMES_EXECUTABLE_TEXT_COLOR", BOLD_GREEN );
   N.text_colors[JAVA] = set_config_str("PLS_FILE_NAMES_JAVA_TEXT_COLOR", YELLOW );
   N.text_colors[JPEG] = set_config_str("PLS_FILE_NAMES_JAVASCRIPT_TEXT_COLOR", MAGENTA );
   N.text_colors[JAVASCRIPT] = set_config_str("PLS_FILE_NAMES_JAVASCRIPT_TEXT_COLOR", MAGENTA );
   N.text_colors[JULIA] = set_config_str("PLS_FILE_NAMES_JULIA_TEXT_COLOR", MAGENTA );
   N.text_colors[LICENSE] = set_config_str("PLS_FILE_NAMES_LICENSE_TEXT_COLOR", WHITE );
   N.text_colors[LUA] = set_config_str("PLS_FILE_NAMES_LUA_TEXT_COLOR", CYAN );
   N.text_colors[MAKEFILE] = set_config_str("PLS_FILE_NAMES_MAKEFILE_TEXT_COLOR", BOLD_WHITE );
   N.text_colors[MP3] = set_config_str("PLS_FILE_NAMES_MP3_TEXT_COLOR", CYAN );
   N.text_colors[MP4] = set_config_str("PLS_FILE_NAMES_MP4_TEXT_COLOR", CYAN );
   N.text_colors[OBJECT_FILE] = set_config_str("PLS_FILE_NAMES_OBJECT_FILE_TEXT_COLOR", YELLOW );
   N.text_colors[PDF] = set_config_str("PLS_FILE_NAMES_PDF_TEXT_COLOR", WHITE );
   N.text_colors[PLAIN_TEXT] = set_config_str("PLS_FILE_NAMES_PLAIN_TEXT_TEXT_COLOR", WHITE );
   N.text_colors[PNG] = set_config_str("PLS_FILE_NAMES_PNG_TEXT_COLOR", MAGENTA );
   N.text_colors[PYTHON] = set_config_str("PLS_FILE_NAMES_PYTHON_TEXT_COLOR", MAGENTA );
   N.text_colors[RUST] = set_config_str("PLS_FILE_NAMES_RUST_TEXT_COLOR", GREEN );
   N.text_colors[VIM] = set_config_str("PLS_FILE_NAMES_VIM_TEXT_COLOR", GREEN );
   N.text_colors[NVIM] = set_config_str("PLS_FILE_NAMES_NVIM_TEXT_COLOR", GREEN );
   N.text_colors[WAV] = set_config_str("PLS_FILE_NAMES_WAV_TEXT_COLOR", BLUE  );
   N.text_colors[DEFAULT] = set_config_str("PLS_FILE_NAMES_DEFAULT_TEXT_COLOR", WHITE);

   return N;
}



Pretty_File_Time new_pretty_file_time() {
   Pretty_File_Time T;

   T.format = new_format(
      getenv("PLS_TIME_PAD_LEFT"),
      getenv("PLS_TIME_PAD_RIGHT"),
      getenv("PLS_TIME_ICON_GAP")
   );

   T.wrapper = new_wrapper(
      getenv("PLS_TIME_LEFT_WRAPPER"),
      getenv("PLS_TIME_RIGHT_WRAPPER"),
      getenv("PLS_TIME_WRAPPER_PAD_LEFT"),
      getenv("PLS_TIME_WRAPPER_PAD_RIGHT"),
      getenv("PLS_TIME_WRAPPER_BACKGROUND")
   );

   T.icon = set_config_str("PLS_TIME_ICON", GREEN"" );
   T.text_color = set_config_str("PLS_TIME_TEXT_COLOR", GREEN );
   T.format_string = set_config_str("PLS_TIME_FORMAT_STRING", "%b %d" );

   return T;
}




Prettyls_Config new_prettyls_config() {
   Prettyls_Config D;

   D.permissions_config = new_pretty_permissions();
   D.file_size_config = new_pretty_file_size();
   D.file_owner_config = new_pretty_file_owner();
   D.file_name_config = new_pretty_file_name();
   D.file_time_config = new_pretty_file_time();

   return D;
}


void get_permissions(int buf[], struct stat status) {

   for (int i = 0; i < 9; i++) {
      buf[i] = permissions_bits[i] & status.st_mode;
   }
}


char *get_owner(struct stat status) {
   struct passwd *p;
   p = getpwuid(status.st_uid);

   return p->pw_name;
}



void get_file_size(char *buf, struct stat status) {

   int file_size = status.st_size;
   int num_digits = snprintf(NULL, 0, "%d", file_size);

   int idx = 0;
   char *units = file_size_units[0];

   while (num_digits > 3) {
      file_size /= 1000;
      num_digits -= 3;
      units = file_size_units[idx];
      idx++;
   }

   snprintf(buf, num_digits + 1, "%d", file_size);
   strcat(buf, units);
}



int get_file_type(struct stat status, char *file_name) {

   if (S_ISBLK(status.st_mode))  return BLOCK_DEVICE;
   if (S_ISDIR(status.st_mode))  return DIRECTORY;
   if (S_ISCHR(status.st_mode))  return CHARACTER_DEVICE;
   if (S_ISFIFO(status.st_mode)) return FIFO;
   if (S_ISLNK(status.st_mode))  return SYMLINK;
   if (S_ISSOCK(status.st_mode)) return SOCKET;
   if (S_IXOTH & status.st_mode) return EXECUTABLE;

   char *extension = strrchr(file_name, '.');
   if (extension != NULL) {
      extension++;

      if (strcmp(extension, "jpeg") == 0) return JPEG;
      if (strcmp(extension, "c") == 0)    return C;
      if (strcmp(extension, "cpp") == 0)  return CPP;
      if (strcmp(extension, "conf") == 0) return CONF;
      if (strcmp(extension, "java") == 0) return JAVA;
      if (strcmp(extension, "js") == 0)   return JAVASCRIPT;
      if (strcmp(extension, "jl") == 0)   return JULIA;
      if (strcmp(extension, "o") == 0)    return OBJECT_FILE;
      if (strcmp(extension, "rs") == 0)   return RUST;
      if (strcmp(extension, "lua") == 0)  return LUA;
      if (strcmp(extension, "mp3") == 0)  return MP3;
      if (strcmp(extension, "mp4") == 0)  return MP4;
      if (strcmp(extension, "png") == 0)  return PNG;
      if (strcmp(extension, "pdf") == 0)  return PDF;
      if (strcmp(extension, "py") == 0)   return PYTHON;
      if (strcmp(extension, "txt") == 0)  return PLAIN_TEXT;
      if (strcmp(extension, "vim") == 0)  return VIM;
      if (strcmp(extension, "nvim") == 0) return NVIM;
      if (strcmp(extension, "wav") == 0)  return WAV;
      return DEFAULT;
   }

   if (strcmp(file_name, "LICENSE") == 0)  return LICENSE;
   if (strcmp(file_name, "Makefile") == 0) return MAKEFILE;
   return DEFAULT;
}




// docs: https://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
void get_time(char *buf, char *format, struct stat status) {
   struct tm *last_modified = localtime(&status.st_mtime);
   strftime(buf, sizeof(buf), format, last_modified);
}



void print_file_name(char *file_name, int file_type, Pretty_File_Name *N, int column_width) {
   int name_len = strlen(file_name);
   int column_len_remainder = column_width - name_len;

   printf("%-*s", N->format.pad_left, "");
   wrapper_start(&(N->wrapper));
   printf(
      "%s%-*s%s%s",
      N->icons[file_type],
      N->format.icon_gap,
      "",
      N->text_colors[file_type],
      file_name
   );
   wrapper_end(&(N->wrapper));
   printf("%-*s", N->format.pad_right + column_len_remainder, "");
   //printf(
   //   "%s%-*s%s%-*s",
   //   N->icons[file_type],
   //   N->format.icon_gap,
   //   "",
   //   N->text_colors[file_type],
   //   column_width, file_name
   //);
}



void print_just_names(char *file_name, int file_type, Pretty_File_Name *N) {

   wrapper_start(&(N->wrapper));
   printf("%s%-*s%s%s", N->icons[file_type], N->format.icon_gap, "", N->text_colors[file_type], file_name);
   wrapper_end(&(N->wrapper));
   printf("%-*s", N->format.pad_right, "");
}



void print_time(char *time, Pretty_File_Time *T, int column_width) {

   printf("%-*s", T->format.pad_left, "");
   wrapper_start(&(T->wrapper));

   printf(
      "%s%-*s%s%-*s",
      T->icon,
      T->format.icon_gap,
      "",
      T->text_color,
      column_width,
      time
   );

   wrapper_end(&(T->wrapper));
   printf("%-*s", T->format.pad_right, "");
}




void update_column_width(File_Stats *current_file, Directory_Stats *current_dir) {

   if (strlen(current_file->file_name) > current_dir->max_file_name_strlen) {
      current_dir->max_file_name_strlen = strlen(current_file->file_name);
   }

   if (strlen(current_file->file_owner) > current_dir->max_file_owner_strlen) {
      current_dir->max_file_owner_strlen = strlen(current_file->file_owner);
   }

   if (strlen(current_file->file_size) > current_dir->max_file_size_strlen) {
      current_dir->max_file_size_strlen = strlen(current_file->file_size);
   }

   if (strlen(current_file->time) > current_dir->max_time_strlen) {
      current_dir->max_time_strlen = strlen(current_file->time);
   }

}




void wrapper_start(Wrapper *w) {
   printf(
      "%s%s%s%-*s",
      RESET,
      w->left_icon,
      w->background_color,
      w->pad_left,
      ""
   );
}

void wrapper_end(Wrapper *w) {
   printf(
      "%-*s%s%s%s",
      w->pad_right,
      "",
      RESET,
      w->right_icon,
      RESET
   );
}





void print_permissions(int bits[], Pretty_File_Permissions *P) {
   printf("%-*s", P->format.pad_left, "");
   wrapper_start(&(P->wrapper));
   printf("%s%-*s%s", P->icon, P->format.icon_gap, "", BOLD_OFF);

   for (int i = 0; i < 9; i++) {
      printf("%s", bits[i] ? P->bit_icons[i % 3] : P->bit_icons[3]);
   }

   wrapper_end(&(P->wrapper));
   printf("%-*s", P->format.pad_right, "");
}



void print_file_size(char *file_size_str, Pretty_File_Size *S, int column_width) {

   printf("%-*s", S->format.pad_left, "");
   wrapper_start(&(S->wrapper));

   printf(
      "%s%-*s%s%-*s",
      S->icon,
      S->format.icon_gap,
      "",
      S->text_color,
      column_width,
      file_size_str
   );

   wrapper_end(&(S->wrapper));
   printf("%-*s", S->format.pad_right, "");
}



void print_file_owner(char *name, Pretty_File_Owner *O, int column_width) {
   char *icon;
   char *color;
   Wrapper w;

   if (strcmp(name, "root") == 0) {
      icon = O->root_icon;
      color = O->root_text_color;
      w = O->root_wrapper;

   } else if (strcmp(name, getenv("USER")) == 0) {
      icon = O->self_icon;
      color = O->self_text_color;
      w = O->self_wrapper;

   } else {
      icon = O->default_icon;
      color = O->default_text_color;
      w = O->default_wrapper;
   }

   printf("%-*s", O->format.pad_left, "");
   wrapper_start(&w);
   printf(
      "%s%-*s%s%-*s",
      icon,
      O->format.icon_gap,
      "",
      color,
      column_width,
      name
   );
   wrapper_end(&w);
   printf("%-*s", O->format.pad_right, "");

}


#endif // PLS_C
