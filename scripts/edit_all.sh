#! /usr/bin/env zsh

files=(
   "src/main.c"
   "src/prettyls.c"
   "include/prettyls.h"
   "configs/config_1.sh"
   "Makefile"
)

nvim -p ${files[@]}
