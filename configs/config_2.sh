#! /usr/bin/env zsh

source $HOME/Code/C/prettyls/scripts/colors.sh

declare -A permissions
declare -A file_size
declare -A file_owner
declare -A file_time



permissions=(
   PAD_LEFT  1

   ICON        "\e[8m"
   ICON_GAP    0
   READ_BIT    "${blue}"
   WRITE_BIT   "${green}"
   EXECUTE_BIT "${cyan}"
   NONE_BIT    "${magenta}∙"
)


file_size=(
   PAD_LEFT   1
   ICON_GAP   0

   ICON       "${yellow}猪"
   TEXT_COLOR "${magenta}"
)


file_owner=(
   ICON_GAP   1
   ROOT_ICON       "${magenta} "
   SELF_ICON       "${magenta} "
   DEFAULT_ICON     "${magenta} "
   SELF_TEXT_COLOR  "${green}"
   ROOT_TEXT_COLOR    "${green}"
   DEFAULT_TEXT_COLOR "${green}"
)


file_name=(
   PAD_LEFT 0

   DIR_TEXT_COLOR  "${magenta}"
   EXECUTABLE_TEXT_COLOR  "${blue}"
   OBJECT_FILE_TEXT_COLOR  "${cyan}"
)


file_time=(
   PAD_RIGHT 2
   ICON  "${cyan} "
   TEXT_COLOR  "${yellow}"
)



for key value in ${(kv)permissions}; do
   export PLS_PERMISSIONS_$key=$(echo -e "$value")
done

for key value in ${(kv)file_size}; do
   export PLS_FILE_SIZE_$key=$(echo -e "$value")
done

for key value in ${(kv)file_owner}; do
   export PLS_FILE_OWNER_$key=$(echo -e "$value")
done

for key value in ${(kv)file_name}; do
   export PLS_FILE_NAMES_$key=$(echo -e "$value")
done

for key value in ${(kv)file_time}; do
   if [[ $key == 'FORMAT_STRING' ]]; then
      export PLS_TIME_$key=$value
   else
      export PLS_TIME_$key=$(echo -e "$value")
   fi
done




















