
#ifndef PRETTYLS_H
#define PRETTYLS_H
#include <sys/stat.h>




enum Filetype {
   BLOCK_DEVICE,
   CHARACTER_DEVICE,
   DIRECTORY,
   FIFO,
   SYMLINK,
   SOCKET,
   C,
   CPP,
   CONF,
   DOSINI,
   EXECUTABLE,
   JAVA,
   JAVASCRIPT,
   JPEG,
   JULIA,
   LICENSE,
   LUA,
   MAKEFILE,
   MP3,
   MP4,
   OBJECT_FILE,
   PNG,
   PDF,
   PYTHON,
   PLAIN_TEXT,
   RUST,
   VIM,
   NVIM,
   WAV,
   DEFAULT
};


typedef struct File_Stats {
   int permission_bits[9];
   char file_size[16];
   char time[32];
   char *file_name;
   enum Filetype file_type;
   char *file_owner;

} File_Stats;


typedef struct Directory_Stats {
   int size;
   int num_dirs;
   int num_dotdirs;
   int num_files;
   int num_dotfiles;
   int max_file_size_strlen;
   int max_file_name_strlen;
   int max_file_owner_strlen;
   int max_time_strlen;

   int *directory_indices;
   int *file_indices;

} Directory_Stats;



typedef struct Format {
   char *align;
   int pad_left;
   int pad_right;
   int icon_gap;

} Format;
   

typedef struct Wrapper {
   int pad_left;
   int pad_right;

   char *left_icon;
   char *right_icon;
   char *background_color;

} Wrapper; 


typedef struct Pretty_File_Permissions {
   Wrapper wrapper;
   Format format;
   char *icon;
   char *bit_icons[4];

} Pretty_File_Permissions;


typedef struct Pretty_File_Size {
   Wrapper wrapper;
   Format format;
   char *icon;
   char *text_color;

} Pretty_File_Size;


typedef struct Pretty_File_Owner {
   Format format;
   Wrapper self_wrapper;

   char *self_icon;
   char *self_text_color;

   Wrapper root_wrapper;
   char *root_icon;
   char *root_text_color;

   Wrapper default_wrapper;
   char *default_icon;
   char *default_text_color;

} Pretty_File_Owner;




typedef struct Pretty_File_Name {
   Format format;
   Wrapper wrapper;

   char *icons[DEFAULT + 1];
   char *text_colors[DEFAULT + 1];

   char *dir_icon;
   char *reg_file_icon;
   char *symlink_icon;
   char *blk_device_icon;
   char *executable_icon;
   char *o_icon;
   char *makefile_icon;
   char *default_icon;

   char *dir_text_color;
   char *reg_file_text_color;
   char *symlink_text_color;
   char *blk_device_text_color;
   char *executable_text_color;
   char *o_text_color;
   char *makefile_text_color;
   char *default_text_color;

} Pretty_File_Name;



typedef struct Pretty_File_Time {
   Format format;
   Wrapper wrapper;

   char *format_string;
   char *icon;
   char *text_color;

} Pretty_File_Time;



typedef struct Prettyls_Config {
   Pretty_File_Permissions permissions_config;
   Pretty_File_Owner file_owner_config;
   Pretty_File_Size file_size_config;
   Pretty_File_Name file_name_config;
   Pretty_File_Time file_time_config;

} Prettyls_Config;





// initializers
Directory_Stats new_directory(char *path);
Prettyls_Config new_prettyls_config();
Pretty_File_Permissions new_pretty_permissions();
Pretty_File_Size new_pretty_file_size();
Pretty_File_Owner new_pretty_file_owner();
Pretty_File_Name new_pretty_file_name();
Pretty_File_Time new_pretty_file_time();
Format new_format(char *pad_left, char *pad_right, char *icon_gap);
Wrapper new_wrapper(
   char *left_icon,
   char *right_icon,
   char *pad_left,
   char *pad_right,
   char *background_color
);

// data aquisition
void get_permissions(int buf[], struct stat status);
void get_file_size(char *buf, struct stat status);
void get_time(char *buf, char *format, struct stat status);
char *get_owner(struct stat status);
int get_file_type(struct stat status, char *file_name);

// printing and formatting
char *set_config_str(char *user_config_str, char *default_str);
void update_column_width(File_Stats *current_file, Directory_Stats *current_dir);
void wrapper_start(Wrapper *w);
void wrapper_end(Wrapper *w);
void print_pretty_stat(Wrapper *w, char *icon, char *text_color, char *text, int column_width);
void print_permissions(int bits[], Pretty_File_Permissions *P);
void print_file_size(char *size_str, Pretty_File_Size *S, int column_width);
void print_file_owner(char *name, Pretty_File_Owner *O, int column_width);
void print_file_name(char *file_name, int file_type, Pretty_File_Name *N, int column_width);
void print_just_names(char *file_name, int file_type, Pretty_File_Name *N);
void print_time(char *time, Pretty_File_Time *T, int column_width);


#endif // PRETTYLS_H
