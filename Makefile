
CC = gcc
CFLAGS = -Wall

BIN = prettyls
SRCDIR = src
INCDIR = include
DESTDIR = /usr/local/bin

SRCS = $(wildcard $(SRCDIR)/*.c)
OBJS = $(patsubst $(SRCDIR)/%.c, %.o, $(SRCS))


# build
all: $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ 

%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -I $(INCDIR) -o $@

clean:
	rm -f *.o $(BIN)

install:
	sudo install -t $(DESTDIR) $(BIN)
