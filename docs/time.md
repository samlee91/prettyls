
# Time Format String

| **Specifier** |      **Description**      |   **Example**   |
|:--------------|:--------------------------|:----------------|
| %a	| Short weekday name| 	Sun
| %A	| Long weekday name| 	Sunday
| %b	| Short month name| 	Mar
| %B	| Long month name| 	March
| %c	| Date and time | Sun Aug 19 02:56:02 2012
| %d	| Day of the month |	19
| %H	| Hour in 24h format |	14
| %I	| Hour in 12h format |	05
| %j	| Day of the year |	231
| %m	| Month as a decimal number (01-12)	08
| %M	| Minute (00-59)	55
| %p	| AM or PM designation	PM
| %S	| Second (00-61)	02
| %U	| Week number (Sun = 00) |	33
| %w	| Weekday  (0-6)|	4
| %W	| Week number (Mon = 00) | 	34
| %x	| Date representation |	08/19/12
| %X	| Time representation |	02:50:06
| %y	| Year (two digits) | 01
| %Y	| Year	| 2022
| %Z	| Timezone name or abbreviation	| CDT
| %%	| A % sign	|%
