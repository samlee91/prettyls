#! /usr/bin/env zsh

declare -A permissions
declare -A file_size
declare -A file_owner
declare -A file_name
declare -A file_time

# foreground colors
black="\e[30m"
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
blue="\e[34m"
magenta="\e[35m"
cyan="\e[36m"
white="\e[37m"

# background colors
red_bg="\e[41m"
green_bg="\e[42m"
yellow_bg="\e[43m"
blue_bg="\e[44m"
magenta_bg="\e[45m"
cyan_bg="\e[46m"



permissions=(
   PAD_LEFT  1
   PAD_RIGHT 0
   ICON_GAP  1
   ICON "${black} "

   LEFT_WRAPPER "${yellow}"
   RIGHT_WRAPPER "${yellow}"
   WRAPPER_BACKGROUND "${yellow_bg}"
   WRAPPER_PAD_LEFT  1
   WRAPPER_PAD_RIGHT 0

   READ_BIT "${black}"
   WRITE_BIT "${black}"
   EXECUTE_BIT "${black}"
   NONE_BIT "${black}∙"
)


file_size=(
   PAD_RIGHT 0
   PAD_LEFT  0
   ICON_GAP  0
   ICON "${black}猪"
   TEXT_COLOR "${black}"

   LEFT_WRAPPER  "\e[30;45m"
   RIGHT_WRAPPER  "${magenta}"
   WRAPPER_BACKGROUND  "${magenta_bg}"
   WRAPPER_PAD_LEFT   1
   WRAPPER_PAD_RIGHT  0
)


file_owner=(
   ICON_GAP  1
   PAD_LEFT  0
   PAD_RIGHT 0
   ROOT_ICON  "${black} "
   SELF_ICON  "${black} "
   DEFAULT_ICON  "${black} "
   SELF_TEXT_COLOR  "${black}"
   ROOT_TEXT_COLOR  "${black}"
   DEFAULT_TEXT_COLOR "${black}"

   SELF_LEFT_WRAPPER  "${blue}"
   SELF_RIGHT_WRAPPER  "${blue}"
   SELF_WRAPPER_BACKGROUND  "${blue_bg}"
   SELF_WRAPPER_PAD_LEFT  1
   SELF_WRAPPER_PAD_RIGHT 0

   ROOT_LEFT_WRAPPER  "${red}"
   ROOT_RIGHT_WRAPPER   "${red}"
   ROOT_WRAPPER_BACKGROUND  "${red_bg}"
   ROOT_WRAPPER_PAD_LEFT  0
   ROOT_WRAPPER_PAD_RIGHT 0

   DEFAULT_LEFT_WRAPPER  "${cyan}"
   DEFAULT_RIGHT_WRAPPER   "${cyan}"
   DEFAULT_WRAPPER_BACKGROUND "${cyan_bg}"
   DEFAULT_WRAPPER_PAD_LEFT  0
   DEFAULT_WRAPPER_PAD_RIGHT 0
)


file_name=(
   PAD_LEFT   0
   PAD_RIGHT  3

   WRAPPER_PAD_RIGHT  1
   LEFT_WRAPPER "\e[30;46m"
   RIGHT_WRAPPER  "${cyan}  "
   WRAPPER_BACKGROUND "${cyan_bg}"

   DIR_ICON  "${black} "
   SYMLINK_ICON  "${black} "
   SOCKET_ICON  "${black}ﳧ "
   BLOCK_DEVICE_ICON  "${black} " 
   CHARACTER_DEVICE_ICON  "${black}ﭛ " 
   FIFO_ICON  "${black}ﳤ "

   C_ICON  "${black}"
   CPP_ICON "${black}"
   CONF_ICON "${black}"
   DOSINI_ICON "${black}"
   EXECUTABLE_ICON  "${black} " 
   JAVA_ICON "${black}"
   JAVASCRIPT_ICON "${black}"
   JPEG_ICON "${black} "
   JULIA_ICON "${black} "
   LICENSE_ICON "${black} "
   LUA_ICON "${black} "
   MAKEFILE_ICON  "${black} " 
   MP3_ICON "${black}"
   MP4_ICON "${black}"
   OBJECT_FILE_ICON  "${black}ﰹ " 
   PDF_ICON "${black} "
   PNG_ICON "${black} "
   PYTHON_ICON "${black} "
   PLAIN_TEXT_ICON "${black} "
   RUST_ICON "${black} "
   VIM_ICON "${black} "
   NVIM_ICON "${black} "
   WAV_ICON "${black}ﱘ "
   DEFAULT_ICON  "${black} "

   DIR_TEXT_COLOR  "${black}"
   SYMLINK_TEXT_COLOR  "${black}"
   SOCKET_TEXT_COLOR  "${black}"
   BLOCK_DEVICE_TEXT_COLOR  "${black}" 
   CHARACTER_DEVICE_TEXT_COLOR  "${black}" 
   FIFO_TEXT_COLOR  "${black}"

   C_TEXT_COLOR  "${black}"
   CPP_TEXT_COLOR "${black}"
   CONF_TEXT_COLOR "${black}"
   DOSINI_TEXT_COLOR "${black}"
   EXECUTABLE_TEXT_COLOR  "${black}" 
   JAVA_TEXT_COLOR "${black}"
   JAVASCRIPT_TEXT_COLOR "${black}"
   JPEG_TEXT_COLOR "${black}"
   JULIA_TEXT_COLOR "${black}"
   LICENSE_TEXT_COLOR "${black}"
   LUA_TEXT_COLOR "${black}"
   MAKEFILE_TEXT_COLOR  "${black}" 
   MP3_TEXT_COLOR "${black}"
   MP4_TEXT_COLOR "${black}"
   OBJECT_FILE_TEXT_COLOR  "${black}" 
   PDF_TEXT_COLOR "${black}"
   PNG_TEXT_COLOR "${black}"
   PYTHON_TEXT_COLOR "${black}"
   PLAIN_TEXT_TEXT_COLOR "${black}"
   RUST_TEXT_COLOR "${black}"
   VIM_TEXT_COLOR "${black}"
   NVIM_TEXT_COLOR "${black}"
   WAV_TEXT_COLOR "${black}"
   DEFAULT_TEXT_COLOR  "${black}"
)


file_time=(
   PAD_RIGHT  0
   PAD_LEFT   0
   ICON  "${black} "
   FORMAT_STRING  '%b %d'
   TEXT_COLOR  "${black}"
   LEFT_WRAPPER  "\e[30;42m"
   RIGHT_WRAPPER   "${green}"
   WRAPPER_BACKGROUND "${green_bg}"
)




for key value in ${(kv)permissions}; do
   export PLS_PERMISSIONS_$key=$(echo -e "$value")
done

for key value in ${(kv)file_size}; do
   export PLS_FILE_SIZE_$key=$(echo -e "$value")
done

for key value in ${(kv)file_owner}; do
   export PLS_FILE_OWNER_$key=$(echo -e "$value")
done

for key value in ${(kv)file_name}; do
   export PLS_FILE_NAMES_$key=$(echo -e "$value")
done

for key value in ${(kv)file_time}; do
   if [[ $key == 'FORMAT_STRING' ]]; then
      export PLS_TIME_$key=$value
   else
      export PLS_TIME_$key=$(echo -e "$value")
   fi
done

