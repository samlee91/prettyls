# Prettyls
![](screenshot.png)


## Usage
`prettyls` can be executed with or without a path argument, and the `-a` option will show hidden files.

## Configuration
All colors, icons, wrappers, and spacing may be set by the user. The default config and included example configs produce the results in the screenshot above. To use a config, source a config file of a similar form wherever you set your environment variables.

## Installation
Navigate to the desired parent directory and type
```
git clone https://gitlab.com/samlee91/prettyls.git
cd prettyls
make
make install
```
