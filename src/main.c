
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include "prettyls.h"
#include "sort.h"



char *target_directory(int argc, char *argv[]) {
   char *path = ".";

   for (int i = 1; i < argc; i++) {
      if (*argv[i] != '-') {
         path = argv[i];
      }
   }
   return path;
}




void print_stats(Prettyls_Config *config, File_Stats file, Directory_Stats *directory) {

   print_permissions(
      file.permission_bits,
      &(config->permissions_config)
   );

   print_file_owner(
      file.file_owner,
      &(config->file_owner_config),
      directory->max_file_owner_strlen
   );

   print_file_size(
      file.file_size,
      &(config->file_size_config),
      directory->max_file_size_strlen
   );

   print_time(
      file.time,
      &(config->file_time_config),
      directory->max_time_strlen
   );

   //print_name(file.file_name, file.file_type);
   print_file_name(
      file.file_name,
      file.file_type,
      &(config->file_name_config),
      directory->max_file_name_strlen
   );
}



void print_names(Prettyls_Config *config, File_Stats *files, Directory_Stats *directory) {

   for (int i = 0; i < directory->size; i++) {
      //if (files[i].file_name[0] != '.') {
      print_just_names(
         files[i].file_name,
         files[i].file_type,
         &(config->file_name_config)
      );
      printf("\t");
      //}
   }
}


void print_stats_short(Prettyls_Config *config, File_Stats *files, Directory_Stats *directory) {

   for (int i = 0; i < directory->size; i++) {
      if (files[i].file_name[0] != '.') {
         print_stats(config, files[i], directory);
         printf("\n");
      }
   }
}


void print_stats_all_long(Prettyls_Config *config, File_Stats *files, Directory_Stats *directory) {

   for (int i = 0; i < directory->size; i++) {
      print_stats(config, files[i], directory);
      printf("\n");
   }
}








int main(int argc, char *argv[]) {

   char *target_dir = target_directory(argc, argv);
   Prettyls_Config prettyls_config = new_prettyls_config();
   Directory_Stats directory = new_directory(target_dir);
   File_Stats files[directory.size];

   DIR *this_dir = opendir(target_dir);
   char path_buffer[FILENAME_MAX];
   struct stat file_status;
   struct dirent *dir_entry;



   printf("\n");
   for (int i = 0; i < directory.size; i++) {
      dir_entry = readdir(this_dir);

      // get full path
      sprintf(path_buffer, "%s/%s", target_dir, dir_entry->d_name);

      if (stat(path_buffer, &file_status) == -1) {
         perror(path_buffer);
         return 1;
      }

      // load stats
      files[i].file_name = dir_entry->d_name;
      files[i].file_owner = get_owner(file_status);
      files[i].file_type = get_file_type(file_status, files[i].file_name);
      get_permissions(files[i].permission_bits, file_status);
      get_file_size(files[i].file_size, file_status);
      get_time(files[i].time, prettyls_config.file_time_config.format_string, file_status);

      update_column_width(&files[i], &directory);
   }

   closedir(this_dir);


   qsort(files, directory.size, sizeof(File_Stats), compare_file_types);
   void (*print_function)(Prettyls_Config *config, File_Stats *files, Directory_Stats *directory);

   int opt;
   while ((opt = getopt(argc, argv, "al")) != -1) {
      switch (opt) {
         case 'a':
            print_function = &print_stats_all_long;
            break;
         case 'l':
            print_function = &print_stats_all_long;
            break;
         default:
            print_function = &print_stats_short;
            break;
      }
   }

   if (optind == 1) {
      print_function = &print_stats_short;
      //print_function = &print_names;
   }


   print_function(&prettyls_config, files, &directory);
   return 0;
}
