
#ifndef COLORS_H
#define COLORS_H


// controls
#define RESET     "\e[0m"
#define BOLD_OFF  "\e[22m"

// foreground colors
#define BLACK   "\e[30m"
#define RED     "\e[31m"
#define GREEN   "\e[32m"
#define YELLOW  "\e[33m"
#define BLUE    "\e[34m"
#define MAGENTA "\e[35m"
#define CYAN    "\e[36m"
#define WHITE   "\e[37m"

// bold foreground colors
#define BOLD_BLACK   "\e[1;30m"
#define BOLD_RED     "\e[1;31m"
#define BOLD_GREEN   "\e[1;32m"
#define BOLD_YELLOW  "\e[1;33m"
#define BOLD_BLUE    "\e[1;34m"
#define BOLD_MAGENTA "\e[1;35m"
#define BOLD_CYAN    "\e[1;36m"
#define BOLD_WHITE   "\e[1;37m"

// background colors
#define BLACK_BG   "\e[40m"
#define RED_BG     "\e[41m"
#define GREEN_BG   "\e[42m"
#define YELLOW_BG  "\e[43m"
#define BLUE_BG    "\e[44m"
#define MAGENTA_BG "\e[45m"
#define CYAN_BG    "\e[46m"
#define WHITE_BG   "\e[47m"

// bold background colors
#define BOLD_BLACK_BG   "\e[1;40m"
#define BOLD_RED_BG     "\e[1;41m"
#define BOLD_GREEN_BG   "\e[1;42m"
#define BOLD_YELLOW_BG  "\e[1;43m"
#define BOLD_BLUE_BG    "\e[1;44m"
#define BOLD_MAGENTA_BG "\e[1;45m"
#define BOLD_CYAN_BG    "\e[1;46m"
#define BOLD_WHITE_BG   "\e[1;47m"


#endif // COLORS_H
