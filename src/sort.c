#include "sort.h"


int compare_file_types(const void *f1, const void *f2) {
   File_Stats a = *(File_Stats *)f1;
   File_Stats b = *(File_Stats *)f2;

   int ftype_1 = (a.file_name[0] != '.') ? a.file_type : a.file_type + 1;
   int ftype_2 = (b.file_name[0] != '.') ? b.file_type : b.file_type + 1;

   return ftype_1 - ftype_2;
}
