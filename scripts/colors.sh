#! /usr/bin/env zsh


# foreground colors
black="\e[30m"
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
blue="\e[34m"
magenta="\e[35m"
cyan="\e[36m"
white="\e[37m"

# bold foreground colors
Black="\e[1;30m"
Red="\e[1;31m"
Green="\e[1;32m"
Yellow="\e[1;33m"
Blue="\e[1;34m"
Magenta="\e[1;35m"
Cyan="\e[1;36m"
White="\e[1;37m"

# background colors
black_bg="\e[40m"
red_bg="\e[41m"
green_bg="\e[42m"
yellow_bg="\e[43m"
blue_bg="\e[44m"
magenta_bg="\e[45m"
cyan_bg="\e[46m"
white_bg="\e[47m"

# bold background colors
Black_bg="\e[1;40m"
Red_bg="\e[1;41m"
Green_bg="\e[1;42m"
Yellow_bg="\e[1;43m"
Blue_bg="\e[1;44m"
Magenta_bg="\e[1;45m"
Cyan_bg="\e[1;46m"
White="\e[1;47m"


